/*
 * IngToPhy.hpp
 *
 *  Created on: 8 июл. 2019 г.
 *      Author: avatar
 */

#ifndef MATH_ENGTOPHY_HPP_
#define MATH_ENGTOPHY_HPP_


namespace Math
{

  template<class T>
  class EngToPhy
  {
  public:
    EngToPhy(){};
    virtual ~EngToPhy(){};

    void setEngAndPhyLimit(T eng_min,T eng_max,T phy_min,T phy_max)
    {
      this->eng_min = eng_min;
      this->eng_max = eng_max;
      this->phy_min = phy_min;
      this->phy_max = phy_max;

      k = (float)(phy_max-phy_min)/(float)(eng_max-eng_min);
    }
    T operator()(T eng_in)
    {
      if(eng_in<eng_min)
	eng_in = eng_min;
      else
      if(eng_in>eng_max)
      	eng_in = eng_max;

      return (T)(((float)(eng_in-eng_min)*k)+(float)phy_min);
    }
  private:
    T eng_min;
    T eng_max;
    T phy_min;
    T phy_max;
    float k;
  };

}
#endif /* MATH_ENGTOPHY_HPP_ */
