#ifndef MATHQ_H
#define MATHQ_H

#include "stdint.h"


#define M_PI            3.141592653589793       // Число Пи
#define M_SQRT3         1.7320508075688772      // Math.sqrt(3)
#define M_SQRT2         1.414213562373095       // Math.sqrt(2)
// IQ - математика, модуль iq_math.c
typedef int32_t  q24;
typedef int32_t  q20;
typedef int32_t  q19;
typedef int32_t  q16;
typedef int32_t  q15;
typedef int32_t  q8;
typedef int32_t  q6;
//typedef long  q;

//max +-127.9999
#define Q24(A)          (int32_t) ((A) * 16777216.0L)
//max +-2045.9999
#define Q20(A)          (int32_t) ((A) * 1048576.0L)
#define Q19(A)          (int32_t) ((A) * 524288.0L)
#define Q16(A)          (int32_t) ((A) * 65536.0L)
#define Q15(A)          (int32_t) ((A) * 32768.0L)
#define Q8(A)           (int32_t) ((A) * 256.0L)
#define Q6(A)           (int32_t) ((A) * 64.0L)

#define Q24_TO_FLOAT(A) (float)(A/16777216.0f)
#define Q20_TO_FLOAT(A) (float) (A/1048576.0f)
#define Q19_TO_FLOAT(A) (float) (A/524288.0f)
#define Q16_TO_FLOAT(A) (float) (A/65536.0f)
#define Q15_TO_FLOAT(A) (float) (A/32768.0f)
#define Q8_TO_FLOAT(A) (float) (A/256.0f)
#define Q6_TO_FLOAT(A) (float) (A/64.0f)



#define IQ_abs(x) ((x)>=0?(x):-(x))
#define abs(x)    ((x)>=0?(x):-(x))

q24 IQ24_sin(q24 x);
q24 IQ24_cos(q24 x);
q24 IQ24_sqrt(q24 x);
q24 IQ24_div(q24 A, q24 B);
q24 IQ24_atan2(q24 y, q24 x);
// перевод градусов в радианы
// константа grad = [0.0 .. 360.0]
#define IQ24_rad(grad) IQ24((grad)/180.0*M_PI)
// z = sqrt(a*a + b*b);
//iq24 IQ24_module(iq24 a, iq24 b);


#endif // MATHQ_H
