/*
 * FiltrLF.hpp
 *
 *  Created on: 8 июл. 2019 г.
 *      Author: avatar
 */

#ifndef MATH_FILTRLF_HPP_
#define MATH_FILTRLF_HPP_

namespace Math
{

  template<class TYPE>
    class FiltrLF
    {
    public:
      FiltrLF(TYPE set_K, TYPE set_maxK)
      {
	SetK(set_K, set_maxK);
      };
      FiltrLF()
            {

            };
      virtual
      ~FiltrLF()
      {

      };

      TYPE operator ()()
      {
        return out;
      }
      TYPE operator ()(TYPE in)
      {
	out=(TYPE)(K1*(float)out+K2*(float)in);
	return out;
      }

      void
      reset(TYPE reset_to=0)
      {
	out = reset_to;
      }

      void
      SetK(TYPE set_K, TYPE set_maxK)
      {
	//set_max_K = set_maxK;
	//this->set_K = set_K;
	K1 = (float)(set_maxK - set_K) / (float)set_maxK;
	K2 = (float)set_K / (float)set_maxK;
      }

    private:

      TYPE out = 0;
      float K1 = 0; //от старого значения сколько взять
      float K2 = 0; //от нового значения сколько взять
      //TYPE set_K = 0;
      //TYPE set_max_K = 0;

    };

} /* namespace Math */

#endif /* MATH_FILTRLF_HPP_ */
